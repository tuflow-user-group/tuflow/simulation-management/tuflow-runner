
# year, major, minor
version = (2023, 1, 1)

version_text = '-'.join([str(x) for x in version])
