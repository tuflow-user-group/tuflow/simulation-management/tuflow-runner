from PyQt5.QtWidgets import QMessageBox
from PyQt5.QtCore import Qt

from version import version_text

def about_dialog():
    msg_box = QMessageBox()
    msg_box.setTextFormat(Qt.RichText)  # Makes links clickable
    msg_box.setStandardButtons(QMessageBox.Ok)
    msg_box.setIcon(QMessageBox.Information)

    msg_box.setWindowTitle(f"Model Runner {version_text}")

    message = 'Model runner created to run TUFLOW and other models.<br>'
    message += 'Model runner can be freely distributed and modified.<br><br>'

    message += "Application icon provided by <a href='https://icons8.com//'>Icons8</a><br><br>"

    message += "Dialog icon are blue bits provided by <a href='http://www.icojam.com'>IcoJam</a>"

    msg_box.setText(message)

    msg_box.exec()
